<?php
	//create a mysql connection with database server
	$conn = mysqli_connect('localhost','root','');
	//create a database using the mysql connection
	$database = "CREATE DATABASE IF NOT EXISTS db1";
	mysqli_query($conn,$database);
	//select current database
	mysqli_select_db($conn,'db1');
	//create database tables for user info
	$create = "CREATE TABLE IF NOT EXISTS userinfo(
		user_id INT(11) 	NOT NULL auto_increment,
		user_name VARCHAR(50) NOT NULL,
		user_email VARCHAR(100) NOT NULL,
		pass_word VARCHAR(50) NOT NULL,
		uploaded_image VARCHAR(100) NOT NULL,
		UNIQUE(user_name),
		UNIQUE(user_email),
		PRIMARY KEY(user_id)
	)";

	mysqli_query($conn,$create);

?>
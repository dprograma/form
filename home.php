<!DOCTYPE html>
<html>
<head>
	<title>online form</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<form method="post" action="index.php" enctype="multipart/form-data">
		<label for="username">First Name</label><input type="text" name="username" id="username" required></br>
		<label for="email">Email</label><input type="email" name="email" id="email" required></br>
		<label for="password">Password</label><input type="password" name="password" id="password" required></br>
		<label for="reenterpassword">Re-enter password</label><input type="password" name="reenterpassword" 
		id="reenterpassword" required></br>
		<label for="image">Upload photo</label><input type="file" name="image" id="image"></br>
		<input type="submit" name="action" value="Register">
	</form>
</body>
</html>